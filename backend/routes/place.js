const express = require('express');
const { userInfo } = require('os');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const places = bookshelf.Model.extend({
    tableName: 'place',
    idAttribute: 'place_id'
});

const locations = bookshelf.Model.extend({
    tableName: 'location',
    idAttribute: 'location_id'
});

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});

/* GET Places table. */
router.get('/', async function (req, res, next) {
    try {
        const allPlaces = await new places().fetchAll();
        var allPlacesFilled = [];
        for (let i = 0; i < allPlaces.toJSON().length; i++) {

            let location = await new locations().where('location_id', allPlaces.toJSON()[i].tk_location_id).fetch();
            let renter = await new users().where('user_id', allPlaces.toJSON()[i].tk_renter_id).fetch();

            allPlacesFilled.push({
                place: allPlaces.toJSON()[i],
                location: location.toJSON(),
                renter: renter.toJSON()
            })

        }
        res.json(allPlacesFilled);
    }
    catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});
/* GET specific Place */
router.get('/:place_id', async (req, res, next) => {
    try {
        let specificPlace = await new places().where('place_id', req.params.place_id).fetch();
        let specificLocation = await new locations().where('location_id', specificPlace.toJSON().tk_location_id).fetch();
        let renter = await new users().where('user_id', specificPlace.toJSON().tk_renter_id).fetch();

        res.json({
            place: specificPlace.toJSON(),
            location: specificLocation.toJSON(),
            renter: renter.toJSON()
        });
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});
/* GET Event by Renter */
router.get('/byRenter/:tk_id_renter', async (req, res, next) => {
    try {
        let eventByRenter = await new places().where('tk_renter_id', req.params.tk_id_renter).fetchAll();
        const renter = await new users().where('user_id', req.params.tk_id_renter).fetch().toJSON();
        var allPlacesFilled = [];

        for (let i = 0; i < eventByRenter.toJSON().length; i++) {

            let location = await new locations().where('location_id', eventByRenter.toJSON()[i].tk_location_id).fetch();

            allPlacesFilled.push({
                place: eventByRenter.toJSON()[i],
                location: location.toJSON(),
                renter: renter
            })
        }
        res.json(allPlacesFilled);
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});
/* Post new Place.*/
router.post('/', async (req, res, next) => {
    const user = await new users().where('UID',req.body.useruid).fetch();
    try {
        let place1 = {
            tk_location_id: req.body.tk_id_location,
            tk_renter_id: user.toJSON().user_id,
            capacity: req.body.capacity,
            price: req.body.price,
            title: req.body.title,
            description: req.body.description,
            date: req.body.date,
            tk_id_image:req.body.tk_id_image
        }
        let newPlace = await new places().save(place1);
        res.json({ 
            status: "added" ,
            id: newPlace.toJSON().place_id });
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});
/*DELETE specific Place */
router.delete('/:place_id', async (req, res, next) => {
    try {
        const deletedPlace = await new places().where('place_id', req.params.place_id).destroy();
        res.json(deletedPlace.toJSON());
    } catch (error) {
        res.status(500).json(error);
    }
});

module.exports = router;