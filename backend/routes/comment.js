const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);


const myComments = bookshelf.Model.extend({
    tableName: 'comment',
    idAttribute: 'comment_id'
});

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});

const events = bookshelf.Model.extend({
    tableName: 'event',
    idAttribute: 'event_id'
});

/* GET ALL comments. */
router.get('/', async function(req, res, next) {
    try {
        const comments = await new myComments().fetchAll();
        res.json(comments.toJSON());
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET comments by event id */
router.get('/getEvent/:tk_event_id', async function(req, res, next) {
    try {
        
        const comments = await new myComments().where('tk_event_id', req.params.tk_event_id).fetchAll();

        var commentDetails = [];

        for (let i = 0; i < comments.toJSON().length; i++) {

            let event = await new events().where('event_id', comments.toJSON()[i].tk_event_id).fetch();
            let user = await new users().where('user_id', comments.toJSON()[i].tk_user_id).fetch();

            commentDetails.push({
                comment: comments.toJSON()[i],
                user: user.toJSON(),
                event: event.toJSON()    
            })
            
        }
        res.json(commentDetails);

    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* GET comments by user */
router.get('/getUser/:tk_user_id', async function(req, res, next) {
    try {
        const user = await new myComments().where('tk_user_id', req.params.tk_user_id).fetchAll();
        res.json(user.toJSON());
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* POST comment*/
router.post('/', async(req, res, next) => {
    try {
        let comments = req.body;
        const add = await new myComments().save(comments);
        res.json({ status: "added" });
    } catch (error) {
        res.status(500).json(error);
    }
});

/* POST singular comment based on the user uid */
router.post('/:useruid', async(req, res, next) => {

    const user = await new users().where('UID',req.params.useruid).fetch();

    const comment = {
        text: req.body.comment,
        tk_event_id: req.body.tk_event_id,
        tk_user_id: user.toJSON().user_id,
        date: new Date()
    }

    await new myComments().save(comment);
    res.json({status:200})
});

/*DELETE specific comment */
router.delete('/:comment_id', async(req, res, next) => {
    try {
        const deletedComment = await new myComments().where('comment_id', req.params.comment_id).destroy();
        res.json(deletedComment.toJSON());
    } catch (error) {
        res.status(500).json(error);
    }
});




module.exports = router;