const { response } = require('express');
const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const users = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});

const locations = bookshelf.Model.extend({
    tableName: 'location',
    idAttribute: 'location_id'
});

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');    // Lmao?
});

/** GET user based on the UID from firebase */
router.get('/:useruid', async (req, res, next) => {
    try {
        const user = await new users().where('UID', req.params.useruid).fetch();
        const location = await new locations().where('location_id', user.toJSON().tk_id_location).fetch();
        res.json({
            user: user.toJSON(),
            location: location.toJSON()
        })
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});

/** GET user based on the id */
router.get('/id/:useruid', async (req, res, next) => {
    try {
        const user = await new users().where('uid', req.params.useruid).fetch();
        const location = await new locations().where('location_id', user.toJSON().tk_id_location).fetch();
        res.json({
            user: user.toJSON(),
            location: location.toJSON()
        })
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});

/* Change user info */
router.patch("/:user_id", async (req, res, next) => {
    try {
        if (req.body.name != "")
            await new users({ user_id: req.params.user_id }).save({ name: req.body.name }, { patch: true });
        if (req.body.surname != "")
            await new users({ user_id: req.params.user_id }).save({ surname: req.body.surname }, { patch: true });
        if (req.body.gender != "")
            await new users({ user_id: req.params.user_id }).save({ gender: req.body.gender }, { patch: true });
        if (req.body.contact != "")
            await new users({ user_id: req.params.user_id }).save({ contact: req.body.contact }, { patch: true });
        
            await new users({ user_id: req.params.user_id }).save({ tk_id_location: req.body.tk_id_location }, { patch: true });
        
        if (req.body.description != "")
            await new users({ user_id: req.params.user_id }).save({ description: req.body.description }, { patch: true });
        res.json({ status: "patching" });
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }

});


/* Post new Users. */
router.post('/', async (req, res, next) => {

    try {
        let user1 = {
            name: req.body.name,
            surname: req.body.surname,
            email: req.body.email,
            uid: req.body.uid,
            gender: req.body.gender,
            contact: req.body.contact,
            admin: req.body.admin,
            tk_id_location: req.body.tk_id_location,
            tk_id_image: req.body.tk_id_image
        }
        let newUser = await new users().save(user1);
        res.json({ status: "added" });
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});

module.exports = router;