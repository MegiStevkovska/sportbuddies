const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);

const locations = bookshelf.Model.extend({
    tableName: 'location',
    idAttribute: 'location_id'
});
/* GET Location table. */

router.get('/', async function (req, res, next) {
    try {
        const allLocations = await new locations().fetchAll();
        res.json(allLocations.toJSON());
    }
    catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});
/* GET specific Location */
router.get('/:location_id', async (req, res, next) => {
    try {
        let specificLocation = await new locations().where('location_id', req.params.location_id).fetch();
        res.json(specificLocation.toJSON());
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* Post new Location.*/
router.post('/', async (req, res, next) => {

    try {
        let location1 = {
            street: req.body.street,
            street_no: req.body.street_no,
            city: req.body.city
        }
        let newLocation = await new locations().save(location1);
        res.json({ 
            status: "added" ,
            id: newLocation.toJSON().location_id });
    } catch (err) {
        res.status(500).json({ status: "error" });
        console.log(err);
    }
});

/*DELETE specific Location */
router.delete('/:location_id', async (req, res, next) => {
    try {
        const deletedLocation = await new locations().where('location_id', req.params.location_id).destroy();
        res.json(deletedLocation.toJSON());
    } catch (error) {
        res.status(500).json(error);
    }
});

module.exports = router;