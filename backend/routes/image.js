const express = require('express');
const path = require('path');
var router = express.Router();
const multer = require('multer');

const connection = require('./imgdbcon');

const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public_html'),
    filename: function (req, file, cb) {
        // null as first argument means no error
        cb(null, Date.now() + '-' + file.originalname)
    }
})

/* POST Image*/
router.post('/imageupload', async (req, res) => {
    try {
        // 'avatar' is the name of our file input field in the HTML form
        let upload = multer({ storage: storage }).single('avatar');

        upload(req, res, function (err) {
            // req.file contains information of uploaded file
            // req.body contains information of text fields

            if (!req.file) {
                return res.send('Please select an image to upload');
            }
            else if (err instanceof multer.MulterError) {
                return res.send(err);
            }
            else if (err) {
                return res.send(err);
            }

            const classifiedsadd = {
                image: req.file.filename
            };
            const sql = "INSERT INTO images SET ?";
            connection.query(sql, classifiedsadd, (err, results) => {
                if (err) throw err;
                res.json({ success: 1, image_id: results.insertId })

            });
        });
    } catch (err) { console.log(err) }
});

/* GET Image*/
router.get('/:image_id', async (req, res, next) => {
    try {
        const sql = 'SELECT image FROM images WHERE image_id = ?';
        connection.query(sql, req.params.image_id, (err, results) => {
            if (err) throw err;
            if (results[0]) {
                res.json({ success: 1, path: results[0].image })
            }
        });
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});

/* Change user info */
router.patch("/:image_id", async (req, res, next) => {
    try {
        // 'avatar' is the name of our file input field in the HTML form
        let upload = multer({ storage: storage }).single('avatar');

        upload(req, res, function (err) {
            // req.file contains information of uploaded file
            // req.body contains information of text fields

            if (!req.file) {
                return res.send('Please select an image to upload');
            }
            else if (err instanceof multer.MulterError) {
                return res.send(err);
            }
            else if (err) {
                return res.send(err);
            }
            
            const sql = 'UPDATE images SET image = ? WHERE image_id = ?';
                connection.query(sql, [req.file.filename, req.params.image_id], (err, results) => {
                    if (err) throw err;
                    if (results[0]) {
                        res.json({ success: 1, path: results[0].image })
                    }
                });
        });
    } catch (err) { console.log(err) }
});

module.exports = router;