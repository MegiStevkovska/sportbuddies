const express = require('express');
const path = require('path');
var router = express.Router();

var knex = require('./dbcon');

const bookshelf = require('bookshelf')(knex);


const myFavourites = bookshelf.Model.extend({
    tableName: 'favourite',
    idAttribute: 'favourite_id'
});
const myUsers = bookshelf.Model.extend({
    tableName: 'user',
    idAttribute: 'user_id'
});
const mySports = bookshelf.Model.extend({
    tableName: 'sport',
    idAttribute: 'sport_id'
});
/* GET favourites by user */
router.get('/:tk_user_id', async function(req, res, next) {
    try {
        const user = await new myUsers().where('UID',req.params.tk_user_id).fetch();
        const favourite = await new myFavourites().where('tk_user_id', user.toJSON().user_id).fetchAll();
        let allFavsFilled = [];
        for (let i = 0; i < favourite.toJSON().length; i++) {
            let sport = await new mySports().where('sport_id', favourite.toJSON()[i].tk_sport_id).fetch();
            allFavsFilled.push(sport.toJSON());
        }
        res.json(allFavsFilled);
    } catch (error) {
        res.status(500).json({ status: "error", error: error });
    }
});
/* POST favourite*/
router.post('/', async(req, res, next) => {
    try {
        const user = await new myUsers().where('UID',req.body.tk_user_id).fetch();
        const add = await new myFavourites().save({
            tk_user_id: user.toJSON().user_id,
            tk_sport_id: req.body.tk_sport_id
        });
        res.json({ status: "added" });
    } catch (error) {
        res.status(500).json(error);
    }
});
/*DELETE specific favourite */
router.delete('/:tk_user_id/:tk_sport_id', async(req, res, next) => {
    try {
        const user = await new myUsers().where('UID',req.params.tk_user_id).fetch();
        const deletedFavourite = await new myFavourites().where({
            tk_user_id: user.toJSON().user_id,
            tk_sport_id:  req.params.tk_sport_id}).destroy();
        res.json('deleted');
    } catch (error) {
        res.status(500).json(error);
    }
});




module.exports = router;