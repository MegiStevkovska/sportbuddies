import { Button, Stack } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Landing(){
    
    return(
        <Stack gap={2} className="col-lg-3 col-md-4 col-8 m-auto">
            <div className="text-center bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold text-4xl p-2">
                Star your journey as the <br /> Sport Hero <br /> of the neigborhood
            </div>
            <div className="mx-auto text-muted font-medium text-center">
                For the every single time you needed a Buddy. <br /> Now you've found one
            </div>
            <img src={require('../images/logo.png')} alt="Sites logo" className="w-48 sm:w-32 md:w-48 lg:w-64 mx-auto my-4" />
            <div className="mx-auto flex flex-wrap gap-2">
                <Link to="/login" className="btn btn-gradient">
                    Log in now
                </Link>
                <Link to="/register" className='btn btn-outline-purple' >
                    Register an account
                </Link>
            </div>
            <div className="mx-auto mb-2 text-muted font-semibold text-sm text-center mt-2">
                Connect with your city, connect with the people
            </div>
        </Stack> 
    )
}