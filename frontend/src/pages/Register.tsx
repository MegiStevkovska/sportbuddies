
import Button from "react-bootstrap/esm/Button";
import Form from "react-bootstrap/esm/Form";
import Stack from "react-bootstrap/esm/Stack";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom"; import {
    createUserWithEmailAndPassword,
    onAuthStateChanged,
} from "firebase/auth";
import { auth } from "../firebase-config";
import axios from "axios";

export default function Register() {

    const [registerEmail, setRegisterEmail] = useState("");
    const [registerPassword, setRegisterPassword] = useState("");
    const [user, setUser] = useState({});
    const [registerName, setRegisterName] = useState("");
    const [registerSurname, setRegisterSurname] = useState("");
    const [validated, setValidated] = useState(false);
    const [firebaseError, setFirebaseError] = useState("");
    const [registerGender, setRegisterGender] = useState("");
    const [registerContact, setRegisterContact] = useState("");
    
    
    onAuthStateChanged(auth, (currentUser) => {
        if (currentUser != null)
            setUser(currentUser);

    });

    let navigate = useNavigate();

    const register = async () => {
        try {
            setValidated(true);
            const user = await createUserWithEmailAndPassword(
                auth,
                registerEmail,
                registerPassword
            );

            axios.post("users/", { name: registerName,
                surname: registerSurname,
                email: registerEmail,
                uid: user.user.uid,
                gender: registerGender,
                contact: registerContact,
                admin: false,
                tk_id_location: 1,
                tk_id_image: 17
            }).then(function (response) {
                navigate("/user");
            }).catch(function (error) {
                console.log(error);
            });

        } catch (error: any) {
            setFirebaseError(error.message)
        }
    };

    return (
        <Stack gap={2} className="col-lg-3 col-md-4 col-8 m-auto">
            <div className="text-center bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold text-3xl px-2 pb-1">
            Create an account and start your fitness journey
            </div>
            <div className='text-muted text-center mb-2 -mt-2'>It's about time your journey started..</div>
            <Form noValidate validated={validated} className="w-75 mx-auto">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={
                        (event) => { setRegisterEmail(event.target.value) }
                    } required />
                    <Form.Text className="text-muted">
                    </Form.Text>
                    <Form.Control.Feedback type="invalid">
                        Please provide a valid email.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={
                        (event) => { setRegisterPassword(event.target.value) }
                    } required />
                    <Form.Control.Feedback type="invalid">
                        Please provide a strong enough password.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Name" onChange={
                        (event) => { setRegisterName(event.target.value) }
                    } required />
                    <Form.Control.Feedback type="invalid">
                        Please provide a name.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Surname</Form.Label>
                    <Form.Control type="text" placeholder="Surname" onChange={
                        (event) => { setRegisterSurname(event.target.value) }
                    } required />
                    <Form.Control.Feedback type="invalid">
                        Please provide a surname.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Gender</Form.Label>
                    <Form.Control type="text" placeholder="Gender" onChange={
                        (event)=> {setRegisterGender(event.target.value)}
                        }/>
                </Form.Group>       

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Contact number</Form.Label>
                    <Form.Control type="text" placeholder="Contact number" onChange={
                        (event)=> {setRegisterContact(event.target.value)}
                        }/>
                </Form.Group>       
                <div className="invalid-feedback !block">
                    {firebaseError}
                </div>
               <div className="mx-auto flex flex-wrap gap-2 justify-center mt-3">
                    <Button variant="gradient" onClick={register}>
                        Register
                    </Button>
                    <Link to="/login" className='btn btn-outline-purple' >
                        Log into your account
                    </Link>
                </div>
            </Form>
        </Stack>
    )
}