import { Button } from "react-bootstrap";
import { CalendarIcon, OfficeBuildingIcon, UserIcon, CheckCircleIcon } from '@heroicons/react/outline';
import { Link } from "react-router-dom";
import { auth } from "../firebase-config";
import { onAuthStateChanged } from "firebase/auth";
import { useState } from "react";
import { Event, Location, Place, Sport, User } from "../classes";
import axios from "axios";
import React from "react";

interface UserDetails {
    user: User,
    location: Location
}

interface eventDetails {
    event: Event;
    sport: Sport;
    location: Location;
    organizator: User;
}

interface placeDetails {
    place: Place;
    renter: User;
    location: Location;
}

export default function Profile() {

    const [uid, setUid] = useState("");
    const [user, setUser] = useState<UserDetails>();
    const [events, setEvents] = useState<eventDetails[]>();
    const [places, setPlaces] = useState<placeDetails[]>();
    const [avgGrade, setavgGrade] = useState<number>();
    const [imagePath, setImagePath] = useState<String>();
    const [joinedEvents, setJoinedEvents] = useState<eventDetails[]>();

    const getUpcomingEvents = () => {
        axios.get(`/special/${user?.user.user_id}`)
            .then((response) => {
                setJoinedEvents(response.data);
            })
    }
    
    function updateRatings() {
        axios.get(`/rating/getRatings/${user?.user.user_id}`)
            .then((response) => {
                if (response.data.length === 0) {
                    setavgGrade(0);
                } else {
                    var sum = 0;
                    for (var i = 0; i < response.data.length; i++) {
                        sum = sum + response.data[i].rating.grade
                    }
                    setavgGrade(+(sum / response.data.length || 2.5).toFixed(2));
                }
            })
    }

    React.useEffect(() => {
        if (uid != "") {
            updateProfile();
        }
    }, [uid])

    React.useEffect(() => {
        if (user != null) {
            updateEvents();
            updatePlaces();
            updateRatings();
            getUpcomingEvents();
        }
    }, [user])

    onAuthStateChanged(auth, (user) => {
        if (user) {
            setUid(user.uid);
        } else {
            // User not logged in or has just logged out.
        }
    });

    function updateProfile() {
        axios.get(`/users/id/${auth.currentUser?.uid}`)
            .then((response) => {
                setUser(response.data);
                nadjiSliku(response.data.user.tk_id_image)
            });
    }
    function nadjiSliku(idPic: number) {
        axios.get(`image/${idPic}`)
            .then((response) => {
                setImagePath(response.data.path);
            })
    }

    function updateEvents() {
        axios.get(`/event/byOrganizator/${user?.user.user_id}`)
            .then((response) => {
                setEvents(response.data);
            });
    }

    function updatePlaces() {
        axios.get(`/place/byRenter/${user?.user.user_id}`)
            .then((response) => {
                setPlaces(response.data);
            });
    }

    return (
        <>
            <div className="container mx-auto my-5 bordered">
                <div className="lg:flex no-wrap lg:-mx-2" id="drugidiv">

                    <div className="w-full lg:w-3/12 lg:mx-2">

                        <div className="bg-white p-3 border-t-4 border-[#6494ed]">
                            {imagePath ?
                                <div className="image overflow-hidden">
                                    <img className="h-auto w-full mx-auto"
                                        src={'http://localhost:3030/' + imagePath}
                                        alt="Profile Picture" />
                                </div>
                                :
                                <div className="image overflow-hidden">
                                    <img className="h-auto w-full mx-auto"
                                        src="http://localhost:3030/blank-profile-picture-973460_1280.png"
                                        alt="Profile Picture" />
                                </div>}
                            <h1 className="text-gray-900 leading-8 mt-3 mb-1 pb-2 border-b-2 text-[#6494ed]">{user?.user.name} {user?.user.surname}</h1>
                            <p className="text-sm text-gray-500 hover:text-gray-600 leading-6 mt-3 h-[120px] overflow-y-auto scroll-smooth">
                                {user?.user.description || `We dont know much about ${user?.user.name} ${user?.user.surname}, but we are sure they're great!`}
                            </p>
                            <Link to={'/user/editProfile/' + uid} className="no-underline">
                                <Button variant="outline-purple mt-3"
                                    className="w-full p-2">
                                    Edit profile
                                </Button>
                            </Link>
                            <Link to='/user/favourites' className="no-underline">
                                <Button variant="outline-purple mt-3"
                                    className="w-full p-2">
                                    Change favorites
                                </Button>
                            </Link>
                        </div>

                    </div>

                    <div className="w-full lg:w-9/12 lg:mx-2 flex flex-col lg:justify-center ">
                        <div className="bg-white bordered">
                            <div className="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
                                <UserIcon className="h-5 text-[#6494ed]" />
                                <span className="tracking-wide">About</span>
                            </div>
                            <div className="text-gray-700">
                                <div className="grid lg:grid-cols-2 text-sm">
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">First Name</div>
                                        <div className="px-4 py-2">{user?.user.name}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">Last Name</div>
                                        <div className="px-4 py-2">{user?.user.surname}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">Gender</div>
                                        <div className="px-4 py-2">{user?.user.gender}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">Contact No.</div>
                                        <div className="px-4 py-2">{user?.user.contact}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">City</div>
                                        <div className="px-4 py-2">{user?.location.city}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">Address</div>
                                        <div className="px-4 py-2">{user?.location.street} {user?.location.street_no}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">Email</div>
                                        <div className="px-4 py-2">
                                            <a className="text-[#6494ed] no-underline" href={'mailto:' + user?.user.email}>{user?.user.email}</a>
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-semibold">User rating</div>
                                        <div className="px-4 py-2">{avgGrade} / 5</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="my-4"></div>

                        <div className="bg-white bordered">

                            <div className="grid grid-cols-3">
                                <div>
                                    <div className="flex items-center space-x-2 font-semibold text-gray-900 leading-8 mb-3">
                                        <CalendarIcon className="h-5 text-[#6494ed]" />
                                        <span className="tracking-wide">Hosted events</span>
                                    </div>
                                    <ul className="list-inside space-y-2">
                                        {events?.map((event) =>
                                            <li key={event.event.event_id}>
                                                <Link to={'/user/event/' + event.event.event_id} className="text-[#6494ed] no-underline">{event.event.title}</Link>
                                                <div className="text-gray-500 text-xs">{Intl.DateTimeFormat(['ban', 'id']).format(new Date(event.event.date))}</div>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                                <div>
                                    <div className="flex items-center space-x-2 font-semibold text-gray-900 leading-8 mb-3">
                                        <CheckCircleIcon className="h-5 text-[#6494ed]" />
                                        <span className="tracking-wide">Joined events</span>
                                    </div>
                                    <ul className="list-inside space-y-2">
                                        {joinedEvents?.map((event) =>
                                            <li key={event.event.event_id}>
                                                <Link to={'/user/event/' + event.event.event_id} className="text-[#6494ed] no-underline">{event.event.title}</Link>
                                                <div className="text-gray-500 text-xs">{Intl.DateTimeFormat(['ban', 'id']).format(new Date(event.event.date))}</div>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                                <div>
                                    <div className="flex items-center space-x-2 font-semibold text-gray-900 leading-8 mb-3">
                                        <OfficeBuildingIcon className="h-5 text-[#6494ed]" />
                                        <span className="tracking-wide">Places available to rent</span>
                                    </div>
                                    <ul className="list-inside space-y-2">
                                        {places?.map((place) =>
                                            <li key={place.place.place_id}>
                                                <Link to={'/user/place/' + place.place.place_id} className="text-[#6494ed] no-underline">{place.place.title}</Link>
                                                <div className="text-gray-500 text-xs">{Intl.DateTimeFormat(['ban', 'id']).format(new Date(place.place.date))}</div>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}