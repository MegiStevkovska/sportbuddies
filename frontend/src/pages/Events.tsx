import axios from "axios"
import React from "react";
import { Container, Row, Form } from "react-bootstrap";
import Button from "react-bootstrap/esm/Button";
import { Event, Location, Sport, User } from "../classes";
import CheckBoxSport from "../components/CheckBoxSport";
import EventCard from "../components/EventCard";
import { auth } from "../firebase-config";
import { onAuthStateChanged } from "firebase/auth";
import AddEventModal from "../components/AddEventModal";

interface EventDetail {
    event: Event;
    location: Location;
    sport: Sport;
    organizator: User;
}
interface UserDetails {
    user: User,
    location: Location
}

export default function Events() {

    const [isModalOpen, setModalState] = React.useState(false);
    const [event, setEvent] = React.useState(Array<EventDetail>());
    const [temp, setTemp] = React.useState(Array<EventDetail>());
    const [sport, setSport] = React.useState(Array<Sport>());
    const [searchTerm, setSearchTerm] = React.useState("")
    const [Filters, setFilters] = React.useState<number[]>([])
    const [upcomingEvents,setUpcomingEvents] = React.useState(Array<EventDetail>())
    const [uid, setUid] = React.useState("");
    const [user, setUser] = React.useState<UserDetails>();

    React.useEffect(() => {
        getEvents();
        getSports();
        onAuthStateChanged(auth, (user) => {
            if (user) {
                setUid(user.uid);
            } else {
                // User not logged in or has just logged out.
            }
        });
    }, [])

    React.useEffect(() => {
        if (uid != "") {
            updateProfile();
        }
    }, [uid])

    React.useEffect(() => {
        if (user != null) {
            getUpcomingEvents();
        }
    }, [user])

    function updateProfile(){
        axios.get(`/users/id/${auth.currentUser?.uid}`)
            .then((response) => {
                setUser(response.data);
            });
    }

    const getUpcomingEvents = () => {
        axios.get(`/special/${user?.user.user_id}`)
            .then((response) => {
                setUpcomingEvents(response.data);
            })
    }

    const getEvents = () => {
        axios.get("event")
            .then((response) => {
                setEvent(response.data);
                setTemp(response.data);
            })
    }

    const getSports = () => {
        axios.get("sport")
            .then((response) => {
                setSport(response.data);
            })
    }

    const handleFilters = (filters: any) => {
        let newFilters = { ...Filters }
        newFilters = filters;
        setFilters(newFilters)
        let tt = temp.filter((res) => {
            if (filters.includes(res.sport.sport_id)) {
                return res;
            }
        })
        if (newFilters.length != 0) {
            setEvent(tt);
        }
        else {
            setEvent(temp);
        }
    }

    const osvezi= (a:boolean) => {
        setModalState(false); 
        getEvents();
    }

    const handleHideModal = () => setModalState(false);
    const handleShowModal = () => setModalState(true);
    
    return (
        <div className="mx-2 mb-auto flex flex-col">
            <Button variant="gradient" className="mx-auto my-2" onClick={handleShowModal}>Add an event</Button>
            
            <div>   
                <AddEventModal show={isModalOpen} handleClose={handleHideModal} osvezi={osvezi}/>
            </div>
            
            <CheckBoxSport
                sports={sport}
                handleFilters={(filters: any) => handleFilters(filters)}
            />

            <div className="input-group mt-2 w-50 mx-auto">
                <input type="text" className="form-control" placeholder="Enter name of an event or it's location" onChange={event => { setSearchTerm(event.target.value) }}></input>
            </div>

            {  upcomingEvents.length != 0 ? 
            <Container className="mt-4 border border-bottom-0 border-1 rounded-1 px-4 pt-4">
                <h3 className="font-medium bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent">Upcoming events</h3>
                <Row xs={1} sm={1} md={2} xl={3}>
                    {upcomingEvents.map(res => <EventCard eventdetail={res} key={res.event.event_id} />)}
                </Row>
            </Container>
            : <></>
            }
            <Container className="mt-4 border border-bottom-0 border-1 rounded-1 px-4 pt-4">
                <h3 className="font-medium bg-gradient-to-br from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent">All events</h3>
                <Row xs={1} sm={1} md={2} xl={3}>
                    {event.filter((res) => {

                        if (searchTerm == "") {
                            return res;
                        } else if (res.event.title.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase())) {
                            return res;
                        } else if (res.location.city.toLocaleLowerCase() === (searchTerm.toLocaleLowerCase())) {
                            return res;
                        }
                        else {
                            return 0;
                        }

                    }).map(res => <EventCard eventdetail={res} key={res.event.event_id} />)
                    }
                </Row>
            </Container>
        </div>
    )
}

function Moment(arg0: { res: Event; "": any; }) {
    throw new Error("Function not implemented.");
}
