import axios from "axios"
import React from "react";

import { Container, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import EventCard from "../components/EventCard";
import { Event, Location, Sport, User } from "../classes";

interface EventDetail {
    event: Event;
    location: Location;
    sport: Sport;
    organizator: User;
}

export default function SpecificSport() {

    const { sportName } = useParams<{ sportName: string }>();
    const [event, setEvent] = React.useState(Array<EventDetail>());
    const [searchTerm, setSearchTerm] = React.useState("")

    const getEvents = () => {
        axios.get(`event/bySportName/${sportName}`)
            .then((response) => {
                setEvent(response.data);
            })
    }

    React.useEffect(() => {
        getEvents();
    }, [])

    return (
        <div className="mx-2 mt-3 mb-auto">
            <div className="input-group w-50 mx-auto">
                <input type="text" className="form-control" placeholder="Enter name of an event or it's location" onChange={event => { setSearchTerm(event.target.value) }}></input>
            </div>
            <Container className="mt-4 border border-bottom-0 border-1 rounded-1 px-4 pt-4">
                <Row xs={1} sm={1} md={2} xl={3}>
                    {
                        event.filter((res) => {

                            if (searchTerm == "") {
                                return res;
                            } else if (res.event.title.toLocaleLowerCase().includes(searchTerm.toLocaleLowerCase())) {
                                return res;
                            } else if (res.location.city.toLocaleLowerCase() === (searchTerm.toLocaleLowerCase())) {
                                return res;
                            }
                            else {
                                return 0;
                            }

                        }).map(res => <EventCard eventdetail={res} />)
                    }
                </Row>
            </Container>
        </div>
    )
}