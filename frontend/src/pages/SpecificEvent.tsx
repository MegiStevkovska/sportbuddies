import { EventKey } from "@restart/ui/esm/types";
import axios from "axios";
import { onAuthStateChanged } from "firebase/auth";
import React, { useState } from "react";
import {
  Button,
  Card,
  Col,
  Dropdown,
  DropdownButton,
  Modal,
  OverlayTrigger,
  Row,
  Tooltip,
} from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import { Event, Sport, Location, Comment, User } from "../classes";
import AddEventModal from "../components/AddEventModal";
import EditEventModal from "../components/EditEventModal";
import { auth } from "../firebase-config";

interface SpecificEvent {
  event: Event;
  sport: Sport;
  location: Location;
  organizator: User;
}

interface commentDetails {
  comment: Comment;
  user: User;
}

export default function SpecificEvent() {
  const { eventId } = useParams<{ eventId: string }>();

  const [specific, setSpecific] = useState<SpecificEvent>();
  const [commentDetails, setComments] = useState<commentDetails[]>();
  const [userComment, setUserComm] = useState<String>();
  const [disable, setDisable] = React.useState(true);
  const [eventImagePath, setEventImgagePath] = useState<String>();
  const [profileImagePath, setProfileImgagePath] = useState<String>();
  const [eventDate, setEventDate] = React.useState(new Date());
  const [checkEdit, setCheckEdit] = React.useState(false);
  const [userUID, setUserUID] = useState("");
  const [userID, setUserID] = useState(Number);
  const [isModalOpen, setModalState] = React.useState(false);
  const [isSameProfile, setIsSameProfile] = React.useState("");
  const [usersJoined, setUsersJoined] = React.useState<User[]>();

  let d2 = new Date();
  let w = Intl.DateTimeFormat(["ban", "id"]).format(d2);

  React.useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user != undefined) {
        setUserUID(user.uid);
        getUserID();
        checkUserAttendence();
      } else {
        // User not logged in or has just logged out.
      }
    });
  });

  React.useEffect(() => {
    axios.get(`event/${eventId}`).then((response) => {
      setSpecific(response.data!);
      setEventDate(new Date(response.data.event.date));
      nadjiSliku(response.data.event.tk_id_image, "event");
      nadjiSliku(response.data.organizator.tk_id_image, "pfp");
      updateComments();
      checkUserAttendence();
      checkIfFull();
      d2 = response.data.event.date;
      updateComments();
      joinedUsers();
    });
  }, []);

  function updateComments() {
    axios.get("/comment/getEvent/" + eventId).then((response) => {
      setComments(response.data);
    });
  }

  function checkIfFull() {
    axios
      .get("/event/capacity/" + eventId)
      .then((response) => {
        var capacity = response.data;
        axios
          .get("/event_has_users/" + eventId)
          .then((response) => {
            var attendees = response.data;
            if (attendees == capacity.capacity) {
              setDisable(true);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function nadjiSliku(idPic: number, type: string) {
    axios.get(`image/${idPic}`).then((response) => {
      if (type === "event") {
        setEventImgagePath(response.data.path);
      } else if (type === "pfp") {
        setProfileImgagePath(response.data.path);
      }
    });
  }

  let dateEvent = Intl.DateTimeFormat(["ban", "id"]).format(eventDate);

  function postComment() {
    let uid = auth.currentUser?.uid;
    axios
      .post("/comment/" + uid, {
        comment: userComment,
        tk_event_id: eventId,
      })
      .then((response) => {
        if (response.status == 200) {
          updateComments();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  var userAttendence = false;
  const getUserID = async () => {
    await axios.get(`/users/id/${auth.currentUser?.uid}`).then((response) => {
      setUserID(response.data.user.user_id);
    });
  };

  const checkUserAttendence = async () => {
    const uid = userUID;
    await axios
      .get("/event_has_users/" + uid + "/" + eventId)
      .then((response) => {
        if (response.status == 200) {
          userAttendence = response.data;
          if (userID == specific?.organizator.user_id) {
            setCheckEdit(true);
            setIsSameProfile("/user/profile");
          } else {
            setIsSameProfile("/user/profile/" + specific?.organizator.UID);
          }
          if (userAttendence || userID == specific?.organizator.user_id) {
            setDisable(true);
          } else setDisable(false);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function postUserToEvent() {
    let uid = auth.currentUser?.uid;
    axios
      .post("/event_has_users/" + uid, {
        tk_event_id: eventId,
      })
      .then((response) => {
        if (response.status == 200) {
          checkUserAttendence();
          checkIfFull();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function joinedUsers() {
    axios
      .get("event_has_users/users/on_event/" + eventId)
      .then((res) => {
        setUsersJoined(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const osvezi = (a: boolean) => {
    setModalState(false);
    axios.get(`event/${eventId}`).then((response) => {
      setSpecific(response.data!);
      setEventDate(new Date(response.data.event.date));
      nadjiSliku(response.data.event.tk_id_image, "event");
      nadjiSliku(response.data.organizator.tk_id_image, "pfp");
      updateComments();
      checkUserAttendence();
      checkIfFull();
      d2 = response.data.event.date;
      updateComments();
      joinedUsers();
    });
  };

  const handleHideModal = () => setModalState(false);
  const handleShowModal = () => setModalState(true);

  // TODO Optimize the picture part of the page
  return (
    <>
      <div className="p-4 my-5 w-75 mx-auto bordered">
        <h1 className="text-center bg-gradient-to-tr from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold">
          {specific?.event.title}
        </h1>
        <Row gutter={[16, 16]}>
          <Col lg={12} xs={24}>
            <Card
              className="my-3 mx-auto !border-0"
              style={{ maxWidth: "960px" }}
            >
              {eventImagePath ? (
                <Card.Img
                  style={{ maxHeight: "180px", objectFit: "contain" }}
                  className="mx-auto"
                  variant="top"
                  alt={specific?.event.title}
                  src={"http://localhost:3030/" + eventImagePath}
                />
              ) : (
                <Card.Img
                  style={{ maxHeight: "180px", objectFit: "contain" }}
                  className="mx-auto"
                  variant="top"
                  alt={specific?.event.title}
                  src="http://localhost:3030/banner.jpg"
                />
              )}
              <Card.Body>
                <Card.Text className="border-t-2 pt-1">
                  Join {specific?.organizator.name} in {specific?.location.city}
                  !
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
          <Col lg={12} xs={24}>
            <div
              className="my-2 mx-auto flex flex-col gap-2"
              style={{ maxWidth: "960px" }}
            >
              <h5>
                Event <i> {specific?.event.title} </i> is being organized in
              </h5>
              <h6 className="-mt-2">
                <i>
                  {specific?.location.city}, {specific?.location.street}{" "}
                  {specific?.location.street_no}
                </i>
              </h6>
              <div className="text-muted -mt-3">On {dateEvent}</div>
              <Link
                to={"/user/sport/" + specific?.sport.name.toLocaleLowerCase()}
              >
                {specific?.sport.name}
              </Link>
              <div>
                {specific?.event.description ||
                  "Some description about the event (it is currently missing)"}
              </div>
              <div className="text-muted">
                For {specific?.event.capacity} people
              </div>
              <OverlayTrigger
                placement="right"
                overlay={
                  <Tooltip id={`tooltip-right`}>
                    {specific?.organizator.name}
                  </Tooltip>
                }
              >
                {profileImagePath ? (
                  <Link to={isSameProfile} className="w-fit">
                    <img
                      className="rounded-circle"
                      style={{ width: "48px", height: "48px" }}
                      src={"http://localhost:3030/" + profileImagePath}
                    />
                  </Link>
                ) : (
                  <Link to={isSameProfile} className="w-fit">
                    <img
                      className="rounded-circle"
                      style={{ width: "48px", height: "48px" }}
                      src={
                        "http://localhost:3030/blank-profile-picture-973460_1280.png"
                      }
                    />
                  </Link>
                )}
              </OverlayTrigger>

              <div className="text-center mt-2 flex justify-center space-x-3 w-full">
                {eventDate > d2 && (
                  <Button
                    variant="purple"
                    onClick={postUserToEvent}
                    disabled={disable}
                  >
                    Join
                  </Button>
                )}
                {eventDate <= d2 && (
                  <Button variant="purple" disabled={true}>
                    You can no longer join this event
                  </Button>
                )}

                {checkEdit == true && (
                  <>
                    <Button variant="purple" onClick={handleShowModal}>
                      Edit event
                    </Button>
                    <EditEventModal
                      show={isModalOpen}
                      handleClose={handleHideModal}
                      osvezi={(o1: boolean) => osvezi(o1)}
                      current={eventId || ""}
                      data={specific}
                    />
                  </>
                )}

                <DropdownButton
                  title={"View joined users"}
                  key={"end"}
                  drop={"end"}
                  variant="gradient"
                >
                  <Dropdown.ItemText>
                    Users that have already joined {specific?.event.title}
                  </Dropdown.ItemText>
                  <Dropdown.Divider />
                  {(usersJoined || []).map((userJoined) => (
                    <Dropdown.Item eventKey={userJoined.UID} key={userJoined.UID} as="div">
                      {" "}
                      <Link to={"/user/profile/" + userJoined.UID}>
                        {userJoined.name}
                      </Link>
                    </Dropdown.Item>
                  ))}
                </DropdownButton>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className="px-4 my-5 w-75 mx-auto bordered">
        <div className="pt-2">
          <div className="input-group mb-3 mt-2 w-100">
            <input
              type="text"
              className="form-control border-purple"
              placeholder="Add a comment to the event!"
              onChange={(event) => {
                setUserComm(event.target.value);
              }}
            />
            <Button variant="outline-purple" onClick={postComment}>
              Post comment
            </Button>
          </div>
          {commentDetails?.map((commentDetail) => (
            <div key={commentDetail.user.UID} className="border-l-[3px] mb-3 px-2">
              {commentDetail.comment.text} -{" "}
              <Link to={"/user/profile/" + commentDetail.user.UID}>
                {" "}
                {commentDetail.user.name}{" "}
              </Link>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
