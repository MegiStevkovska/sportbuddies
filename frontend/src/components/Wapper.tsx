import { HomeIcon, MenuIcon, UserAddIcon, UserIcon } from "@heroicons/react/solid";
import { auth } from "../firebase-config";
import {onAuthStateChanged} from "firebase/auth";
import { useState } from "react";
import { Button, Form, Nav, Offcanvas, Stack } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { Outlet } from "react-router-dom";

export default function Wrapper(){

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    let navigate = useNavigate();
    
    onAuthStateChanged(auth, (currentUser) => {
        if(currentUser != null){
            navigate("/user");
        }
    });

    const background = 
        "https://images.unsplash.com/photo-1590556409324-aa1d726e5c3c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80";

    return(
        <div className="wrapper-container">
            <main>
            <div className="flex h-screen">
                <div className="w-1/2 sm:flex hidden justify-center text-white bg-no-repeat bg-center bg-[#6495ed]" style={{backgroundImage: "url("+background+")"}}>
                    <button className="absolute left-0" onClick={handleShow}>
                        <MenuIcon className="w-8 m-1 p-0.5 border-2 hover:border-[#6495ed] transition duration-250 ease-in-out rounded-md"/>
                    </button>
                    <div 
                        className="
                            border-3 m-auto p-2 font-semibold w-28 md:w-fit 
                            text-center text-md rounded-lg shadow-lg 
                            hover:border-[#6495ed]
                            transition duration-250 ease-in-out cursor-default
                    ">
                        <Link to="/" className="no-underline text-white">Welcome to SportBuddies!</Link>
                    </div>
                    <Offcanvas show={show} onHide={handleClose}>
                        <Offcanvas.Header closeButton>
                            <Offcanvas.Title as={'h6'} className="bg-gradient-to-tr from-[#6495ED] to-[#F0F8FF] bg-clip-text text-transparent font-bold">Menu</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body className="text-sm flex flex-col justify-start space-y-1">
                                <Nav.Link className="px-0 pt-0" onClick={handleClose}><Link to="/" className="flex flex-row gap-1 hover:gap-2"> <HomeIcon  className="w-5"/> Home</Link></Nav.Link>
                                <Nav.Link className="px-0" onClick={handleClose}><Link to="/login" className="flex flex-row gap-1 hover:gap-2"> <UserIcon  className="w-5"/> Login</Link></Nav.Link>
                                <Nav.Link className="px-0 pb-4 border-b" onClick={handleClose}><Link to="/register" className="flex flex-row gap-1 hover:gap-2"> <UserAddIcon  className="w-5"/> Register</Link></Nav.Link>
                        </Offcanvas.Body>
                    </Offcanvas>
                </div>
                <div className="flex flex-col justify-center w-full">
                    <button className="block sm:hidden absolute left-0 top-0" onClick={handleShow}>
                        <MenuIcon className="w-8 m-1 p-0.5 border-2 rounded-md"/>
                    </button>
                    <div className="flex grow">
                        <Outlet/>
                    </div>
                </div>
            </div>
            </main>
        </div>
    )
}