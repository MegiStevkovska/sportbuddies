import axios from "axios";
import { useEffect, useState } from "react";
import { Button, Card, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Location, Place, Sport, User } from "../classes";

interface props{
    placedetail: {
        place: Place;
        location: Location;
        sport: Sport;
        renter: User;
    }
}

export default function PlaceCard(props:props){

    const [placeImagePath, setPlaceImgagePath] = useState("");

    function nadjiSliku() {
        axios.get(`image/${props.placedetail.place.tk_id_image}`)
            .then((response) => {
                    setPlaceImgagePath('http://localhost:3030/'+response.data.path);
            })
    }    

    useEffect(() => {
        nadjiSliku();
    },[])

    return(
        <Col className="mb-3">
            <Card style={{height:"30rem"}}>
                <Card.Img className="object-cover h-[10rem]" variant="top" src={placeImagePath || ""} />
                <Card.Body>
                    <Card.Title>{props.placedetail.place.title} <Link to={'/user/place/'+props.placedetail.place.place_id.toString()} className="text-muted fs-6 text-decoration-none"> </Link> </Card.Title>
                    <Card.Text className="overflow-y-auto scroll-smooth h-[150px]">
                        {props.placedetail.place.description || "Some description about the event (it is currently missing)"}
                    </Card.Text>
                    <Link to={'/user/place/'+props.placedetail.place.place_id.toString()}><Button className="w-full" variant="purple">See the place</Button></Link>
                </Card.Body>
                <Card.Footer>
                    <small className="text-muted">Starting at: {props.placedetail.place.price}€ a day</small>
                </Card.Footer>
            </Card> 
        </Col>
    )
}