import { onAuthStateChanged, signOut } from "firebase/auth";
import { useState } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { auth } from "../firebase-config";

// eslint-disable-next-line import/no-anonymous-default-export
export default function(){
    
    const [user, setUser] = useState({});
    let navigate = useNavigate();

    const logout = async () => {
        await signOut(auth);
    };
    
    onAuthStateChanged(auth, (currentUser) => {
        if(currentUser == null)
            navigate("/");
            // or use navigate(-1); to navigate to the last page the user visited
    });
    
    // Remember to use /user/route for links since these are the authenticated routes
    return(
        <>
        <Navbar expand="lg">
            <Container className="border-bottom">
                <Navbar.Toggle aria-controls="basic-navbar-nav" className="mx-auto border-0" />
                <Navbar.Collapse id="basic-navbar-nav"className="p-2s">
                
                <Navbar.Brand 
                    className="
                        pb-2 pt-1 hidden lg:block 
                        bg-gradient-to-tl from-[#6495ED] to-[#F0F8FF] bg-clip-text !text-transparent 
                        font-bold cursor-default
                ">
                        Sport Buddies
                </Navbar.Brand>
                
                <Nav className="me-auto text-center">
                    <Nav.Link href='/user'>Home</Nav.Link>
                    <Nav.Link href='/user/events-by-favourites'>Favourites</Nav.Link>
                    <Nav.Link href='/user/place'>Places</Nav.Link>
                    <Nav.Link href='/user/profile'>Your Profile</Nav.Link>
                </Nav>
                <Nav.Link onClick={logout} className='logout no-underline text-center'>Logout</Nav.Link>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        </>
    )
}