import axios from "axios";
import React from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { Sport } from "../classes";

interface Props {
    show: boolean
    handleClose: () => void;
    osvezi: (a: boolean) => void;
    current: string;
    data: any;
}

export default function EditEventModal(props: Props) {

    const [sports, setSports] = React.useState(Array<Sport>());
    const [idLocation, setIdLocation] = React.useState(0);
    const [validated, setValidated] = React.useState(false);
    const [invalidDate, setInvalidDate] = React.useState(false);
    const [event_id, setevent_id] = React.useState("");
    const [tk_id_location, settk_id_location] = React.useState("");
    const [tk_id_sport, settk_id_sport] = React.useState("");
    const [tk_id_organizator, settk_id_organizator] = React.useState("");
    const [tk_id_image, settk_id_image] = React.useState("");
    const [title1, settitle] = React.useState("");
    const [description, setdescription] = React.useState("");
    const [date, setdate] = React.useState("");

    const [capacity, setCapacity] = React.useState("");

    const [imagePath, setImagePath] = React.useState<String>();
    const [userInfo, setuserInfo] = React.useState({
        file: new Blob(),
        filepreview: "",
        lol: false
    });
    let o1 = false;

    const handleInputChange = (event: any) => {
        setuserInfo({
            ...userInfo,
            file: event.target.files[0],
            filepreview: URL.createObjectURL(event.target.files[0]),
            lol: true
        });
    }

    React.useEffect(() => {
        axios.get("sport")
            .then((response) => {
                setSports(response.data);

            })
    }, [])

    const doSomething = (event: any) => {
        event.preventDefault();
        const updateSpecificEvent = (lokacija?: string, picture?: number) => {
            var capacityValue: string = event.target.formCapacity.value;
            if (lokacija == undefined)
                lokacija = "";
            if (sportid == undefined)
                sportid = "";
            if (parseInt(event.target.formCapacity.value) == null)
                capacityValue = "";
            axios.patch(`/event/${props.current}`, {
                event_id: event_id,
                tk_id_location: lokacija,
                tk_id_sport: sportid,
                tk_id_image: tk_id_image,
                capacity: capacityValue,
                title: event.target.formTitle.value,
                description: event.target.formDescription.value,
                date: event.target.dob.value
            }).then((response) => {
                if (response.status == 200) {
                    o1 = true;
                    const formdata = new FormData();
                    if (userInfo.lol === true) {
                        formdata.append('avatar', userInfo.file);
                        axios.patch(`image/${props.data.event.tk_id_image}`, formdata, {
                            headers: { "Content-Type": "multipart/form-data" }
                        })
                    }
                    props.osvezi(o1);
                }
            });
        }
        setValidated(true);
        if (new Date(event.target.dob.value) < new Date()) {
            setInvalidDate(true);
        } else {
            var chosenSport: String = "";
            for (let i = 0; i < event.target.id.length; i++) {
                if (event.target.id[i].checked)
                    chosenSport = (event.target.id[i].value)

            }

            var sportid: string;
            sports.filter((curr) => {
                if (curr.name === chosenSport)
                    sportid = curr.sport_id.toString();
            })
            if (event.target.street.value != "" && event.target.streetNo.value != "" && event.target.streetNo.value) {
                axios.post('/location', {
                    street: event.target.street.value,
                    street_no: event.target.streetNo.value,
                    city: event.target.city.value
                }).then((res) => {
                    if (res.status == 200) {
                        setIdLocation(res.data.id);
                        updateSpecificEvent(res.data.id);
                    } else {
                        updateSpecificEvent(res.data.id);
                    }
                }).catch((error) => {
                    console.log(error);
                })
            }
            else
                updateSpecificEvent();
        }

    }
    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Edit an event</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form className="flex flex-col gap-4" onSubmit={doSomething} noValidate validated={validated}>
                    <Form.Group controlId="formTitle">
                        <Form.Label>Event title</Form.Label>
                        <Form.Control type="text" name="title" placeholder={props.data.event.title} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Sport type</Form.Label>
                        <div className="flex flex-row gap-3 flex-wrap">
                            {sports.map(nm => <Form.Check type="radio" id="id" name="answ" key={nm.sport_id} value={nm.name} label={nm.name} required />)}
                        </div>
                    </Form.Group>
                    <Form.Group className="flex flex-col gap-2">
                        <Form.Label>Enter location info</Form.Label>
                        <Form.Control type="text" id="city" placeholder={props.data.location.city} />
                        <Form.Control type="text" id="street" placeholder={props.data.location.street} />
                        <Form.Control type="text" id="streetNo" placeholder={props.data.location.street_no} />
                    </Form.Group>
                    <Form.Group controlId="formDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" placeholder={props.data.event.description} />
                    </Form.Group>
                    <Form.Group controlId="formCapacity">
                        <Form.Label>Capacity</Form.Label>
                        <Form.Control type="number" placeholder={props.data.event.capacity} />
                    </Form.Group>
                    <Form.Group controlId="dob">
                        <Form.Label>Select Date</Form.Label>
                        <Form.Control type="date" name="dob" placeholder={props.data.event.date} />
                        {invalidDate === true && <Form.Control.Feedback>
                            <p className="text-danger">The date you provided is not correct</p>
                        </Form.Control.Feedback>}
                    </Form.Group>
                    <Form.Group>
                        <div className="flex justify-center items-center w-full">
                            {userInfo.lol ?
                                <img className="previewimg" src={userInfo.filepreview} alt="UploadImage" />
                                :
                                <label htmlFor="dropzone-file" className="flex flex-col justify-center items-center w-full h-64 bg-gray-50 rounded-lg border-2 border-gray-300 border-dashed cursor-pointer dark:hover:bg-bray-100 hover:bg-gray-100 dark:hover:border-gray-100 dark:hover:bg-gray-100">
                                    <div className="flex flex-col justify-center items-center pt-5 pb-6">
                                        <svg className="mb-3 w-10 h-10 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"></path></svg>
                                        <p className="mb-2 text-sm text-gray-500 dark:text-gray-400"><span className="font-semibold">Click to upload</span> or drag and drop</p>
                                        <p className="text-xs text-gray-500 dark:text-gray-400">SVG, PNG, JPG or GIF (MAX. 800x400px)</p>
                                    </div>
                                    <input id="dropzone-file" type="file" className="hidden" onChange={handleInputChange} />
                                </label>}
                        </div>
                    </Form.Group>
                    <Button variant="gradient" type="submit" >
                        Submit
                    </Button>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-secondary" onClick={props.handleClose} className="w-full">
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    )
}