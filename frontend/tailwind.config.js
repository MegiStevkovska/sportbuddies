module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        'lightpurple': '#6494ed'
      }
    },
  },
  plugins: [],
}
