# Praktikum 2
## Sport buddies

## Description
We made an app, that first of all, we though that we need nowadays. While making it we were approving our programming skills that we met this year and  at the same we were thinking about helping our community to be more active. It goes for a platform which provides events of all types, that can be added by registered users. 

## How to use
After registering, on our home page are shown all the events people shared. Also, you as a registered user have an option to add an event of any kind. With our search bar you can easily navigate through all the events, eighter by their name, location and by sport. Also important thing is that, on your profile, you have an option to add your favourite sport, and then in the favourites sector you can see all the events of that sport. 
How can you join the event ? 
By clicking to see the details of the event, all informations you needed are shown. You can also see the capacity of the event and how much people joined so far. If there is still room for you, you are joining by clicking the join button, which will increase the number of joined people. If you chose an event that happened already, you are not allowed to join. Also, you can comment the events, before or after they happened. 
After joining a particular event, you will see it in the upcoming events sector on the home page. 
For all indoor sports, like yoga, pilates etc.. we made a sector where you can share your place for renting, just like forum, where you can add all needed info about it, and your contact can be found on your profile, for someone who is interested. Commenting a rent place is also available.

On your profile, you can update your picture and the main infos. You can also find your average rating. 
Besides your profile, you can also see other user’s profiles, where you can find all the events they hosted and all the places they rent. Also, you can rate other users by entering a grade from 1-5. 

## Requirements
1) Having WAMP 
2) Visual Studio Code
3) MySql
4) Node


## Installation

1.Open PHPMyAdmin panel, log onto your user, create database Praktikum2.  
2.Clone the repository master from gitLab (Visual Studio Code (HTTPS)).  
3.Under routes you will find 2 files : dbcon.js and imgdbcon.js. Configure the connection to fit the user (if you have a password or are using a different user other than root).  
4.Open a new terminal in VS Code  
5.Move to the backend directory (cd backend)  
6.Run `npm i`  
7.Run `node CreateTables.js`  
8.Run `npm start`  
9.Open another terminal in VS Code  
10.Move to the frontend directory (cd frontend)  
11.Run `npm i`  
12.Run `npm start`  
And you are ready!  

## Technologies
React JS, Typescript, Node JS, Express, Firebase


## Functionalities
Add an event, 
Commenting on the event, 
Edit your events, 
Search for an event by location, sport, or name, 
Joining events, 
View your upcoming events, 
Add a place that can be rented, 
Adding favorite sports, 
View all events from selected favorites, 
View organizer's profile, 
Organizer rating, 
View your profile, 
Edit your profile, 
View all the events you've hosted, 
View all the events you've joined, 
See all the places you rent.
You're ready to go!

## Authors
Olga Ivkovic, Ivan Sergejev, Dimitrije Stefanovic, Veronija Kodovska

## Screenshots
![Alt text](https://media.discordapp.net/attachments/984370797583933441/984379721896841246/2.PNG?width=1308&height=604 "Landing page")
![Alt text](https://media.discordapp.net/attachments/984370797583933441/984379722429526036/4.PNG?width=368&height=604 "Adding event modal")
![Alt text](https://media.discordapp.net/attachments/984370797583933441/984379722156883969/3.PNG?width=1202&height=604 "User profile")
![Alt text](https://media.discordapp.net/attachments/984370797583933441/984379722706354206/5.PNG "Favorites sector")
![Alt text](https://cdn.discordapp.com/attachments/984370797583933441/984478201927589888/unknown.png "Home page")



